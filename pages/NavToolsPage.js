
module.exports = {
  wideMenu: {
    slider: "div.slick-list"
  },
  scrollButton: {
    show: "#footer > div.button-scroll-to-top.button-scroll-show"
  }
};
