exports.config = {
    tests: "../../tests/api/*_test.js",
    output: "./output",
    helpers: {
      REST: {
        endpoint: "https://api.eldorado.ua/v1/"
      }
    },
    include: {},
    bootstrap: null,
    mocha: {},
    name: "eldo_codecept",
    plugins: {
      allure: {
        outputDir: "api_report",
        enabled: true
      }
    }
  };
  