const { authPage, navToolsPage, formsPage } = inject();

module.exports = function() {
  return actor({
    login(phone, password) {
      this.amOnPage("/");
      this.waitForElement(navToolsPage.optionList.cabinet, 2);
      this.click(navToolsPage.optionList.cabinet);
      this.fillField(formsPage.signIn.input, phone);
      this.click(formsPage.signIn.authPhone);
      this.waitForElement(formsPage.signIn.submit,3);
      this.fillField(formsPage.signIn.input, password);
      this.click(formsPage.signIn.submit);
      this.waitForInvisible(authPage.modal.dialog, 2);
    }
  });
};
