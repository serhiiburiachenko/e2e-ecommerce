require("dotenv").config();

exports.config = {
  tests: "./tests/**/*_test.js",
  output: "./output",
  helpers: {
    WebDriver: {
      url: "https://eldorado.ua",
      browser: "chrome",
      windowSize: "1920x780",
      smartWait: 3000,
      timeouts: {
        script: 5000,
        "page load": 8000
      },
      keepCookies: true, // keep cookies for all tests
      desiredCapabilities: {
        chromeOptions: {
          args: [
            "--headless",
            "--disable-gpu",
            "--no-sandbox",
            "--disable-extensions",
            "--profile-directory=Default",
            "--incognito",
            "--disable-plugins-discovery",
            "--start-maximized"
          ]
        }
      }
    },
    REST: {
      endpoint: "https://api.eldorado.ua/v1/"
    }
  },
  include: {
    I: "./steps_file.js",    
    navToolsPage: "./pages/NavToolsPage.js",
  },
  bootstrap: null,
  mocha: {},
  name: "eldo_codecept",
  plugins: {
    retryFailedStep: {
      enabled: true
    },
    screenshotOnFail: {
      enabled: true
    },
    wdio: {
      enabled: true,
      services: ["selenium-standalone"]
    },
    allure: {
      enabled: true
    }
  }
};
