const { I, navToolsPage } = inject();
const expect = require("chai").expect;

Feature("Scroll button");

Before(async () => {
  I.amOnPage("/");
  I.waitForVisible(navToolsPage.wideMenu.slider);
});

Scenario.todo("Show scroll button", async I => {
  I.dontSeeElement(navToolsPage.scrollButton.show);
  I.scrollPageToBottom();
  I.waitForVisible(navToolsPage.scrollButton.show);
}).tag("@e2e");

Scenario.todo("Return to top page with scroll button", async I => {
  I.seeElement(".card-item");
  I.scrollPageToBottom();
  I.waitForVisible(navToolsPage.scrollButton.show);
  I.click(navToolsPage.scrollButton.show);
  I.wait(2);
  let { x, y } = await I.grabPageScrollPosition();
  expect(x).to.equal(0);
  expect(y).to.equal(0);
}).tag("@e2e");
