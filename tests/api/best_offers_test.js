const expect = require("chai").expect;
const { I } = inject();

Feature("GET: /best_offers");

Scenario('Verify a successful call', async () => {
    const res = await I.sendGetRequest('best_offers?full_data&limit=5');
    expect(res.status).to.eql(200);
}).tag("@api");

Scenario.todo('Verify a not found call', async () => {
    const res = await I.sendGetRequest('goods_attributes_values_output?conditions=1038940123212213123123');
    expect(res.status).to.eql(404);
}).tag("@api");

